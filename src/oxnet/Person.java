/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxnet;

import java.io.Serializable;


public class Person implements Serializable{


    private String Firstname;
    private String Surname;
    private String Username;
    private int password;
    private int Phone;
    private int Weight;
    private int Height;

    public Person(String Firstname, String Surname, String Username, int password, int Phone, int Weight, int Height) {
        this.Firstname = Firstname;
        this.Surname = Surname;
        this.Username = Username;
        this.password = password;
        this.Phone = Phone;
        this.Weight = Weight;
        this.Height = Height;
    }

    public Person() {
            
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String Firstname) {
        this.Firstname = Firstname;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }

    public int getPhone() {
        return Phone;
    }

    public void setPhone(int Phone) {
        this.Phone = Phone;
    }

    public int getWeight() {
        return Weight;
    }

    public void setWeight(int Weight) {
        this.Weight = Weight;
    }

    public int getHeight() {
        return Height;
    }

    public void setHeight(int Height) {
        this.Height = Height;
    }

    @Override
    public String toString() {
        return "Person{" + "Firstname=" + Firstname + ", Surname=" + Surname + ", Username=" + Username + ", password=" + password + ", Phone=" + Phone + ", Weight=" + Weight + ", Height=" + Height + '}';
    }
    
}


